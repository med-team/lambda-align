#!/usr/bin/env cwl-runner

# This CWL file was automatically generated using CTDConverter.
# Visit https://github.com/WorkflowConversion/CTDConverter for more information.

baseCommand: lambda
class: CommandLineTool
cwlVersion: v1.0
doc: "Lambda is a local aligner optimized for many query sequences and searches in\
  \ protein space. It is compatible to BLAST, but much faster than BLAST and many\
  \ other comparable tools.\nDetailed information is available in the wiki: <https://github.com/seqan/lambda/wiki>\n\
  \n\n\nFor more information, visit http://www.seqan.de"
inputs:
- default: 'false'
  doc: Display the help message with advanced options.
  id: param_full-help
  inputBinding:
    prefix: -full-help
  label: Display the help message with advanced options.
  type:
  - 'null'
  - string
- default: '1'
  doc: 'Turn this option off to disable version update notifications of the application. '
  id: param_version-check
  inputBinding:
    prefix: -version-check
  label: 'Turn this option off to disable version update notifications of the application. '
  type:
  - 'null'
  - string
- default: 'false'
  doc: Display long copyright information.
  id: param_copyright
  inputBinding:
    prefix: -copyright
  label: Display long copyright information.
  type:
  - 'null'
  - string
- default: '1'
  doc: 'Display more/less diagnostic output during operation: 0 [only errors]; 1 [default];
    2 [+run-time, options and statistics].'
  id: param_verbosity
  inputBinding:
    prefix: -verbosity
  label: 'Display more/less diagnostic output during operation: 0 [only errors]; 1
    [default]; 2 [+run-time, options and statistics].'
  type:
  - 'null'
  - int
- doc: Query sequences.
  id: param_query
  inputBinding:
    prefix: -query
  label: Query sequences.
  type: File
- doc: Path to original database sequences (a precomputed index with .sa or .fm needs
    to exist!).
  id: param_database
  inputBinding:
    prefix: -database
  label: Path to original database sequences (a precomputed index with .sa or .fm
    needs to exist!).
  type: File
- default: fm
  doc: database index is in this format.
  id: param_db-index-type
  inputBinding:
    prefix: -db-index-type
  label: database index is in this format.
  type:
  - 'null'
  - string
- default: output.m8
  doc: Filename for output output file
  id: param_output_filename
  inputBinding:
    prefix: -output
  label: Filename for output output file
  type:
  - 'null'
  - string
- default: std
  doc: Print specified column combination and/or order (.m8 and .m9 outputs only);
    call -oc help for more details.
  id: param_output-columns
  inputBinding:
    prefix: -output-columns
  label: Print specified column combination and/or order (.m8 and .m9 outputs only);
    call -oc help for more details.
  type:
  - 'null'
  - string
- default: '0'
  doc: Output only matches above this threshold (checked before e-value check).
  id: param_percent-identity
  inputBinding:
    prefix: -percent-identity
  label: Output only matches above this threshold (checked before e-value check).
  type:
  - 'null'
  - int
- default: '0.1'
  doc: Output only matches that score below this threshold.
  id: param_e-value
  inputBinding:
    prefix: -e-value
  label: Output only matches that score below this threshold.
  type:
  - 'null'
  - double
- default: '500'
  doc: Print at most this number of matches per query.
  id: param_num-matches
  inputBinding:
    prefix: -num-matches
  label: Print at most this number of matches per query.
  type:
  - 'null'
  - int
- default: 'off'
  doc: BAM files require all subject names to be written to the header. For SAM this
    is not required, so Lambda does not automatically do it to save space (especially
    for protein database this is a lot!). If you still want them with SAM, e.g. for
    better BAM compatibility, use this option.
  id: param_sam-with-refheader
  inputBinding:
    prefix: -sam-with-refheader
  label: BAM files require all subject names to be written to the header. For SAM
    this is not required, so Lambda does not automatically do it to save space (especially
    for protein database this is a lot!). If you still want them with SAM, e.g. for
    better BAM compatibility, use this option.
  type:
  - 'null'
  - string
- default: uniq
  doc: Write matching DNA subsequence into SAM/BAM file (BLASTN). For BLASTX and TBLASTX
    the matching protein sequence is "untranslated" and positions retransformed to
    the original sequence. For BLASTP and TBLASTN there is no DNA sequence so a "*"
    is written to the SEQ column. The matching protein sequence can be written as
    an optional tag, see --sam-bam-tags. If set to uniq than the sequence is omitted
    iff it is identical to the previous match's subsequence.
  id: param_sam-bam-seq
  inputBinding:
    prefix: -sam-bam-seq
  label: Write matching DNA subsequence into SAM/BAM file (BLASTN). For BLASTX and
    TBLASTX the matching protein sequence is "untranslated" and positions retransformed
    to the original sequence. For BLASTP and TBLASTN there is no DNA sequence so a
    "*" is written to the SEQ column. The matching protein sequence can be written
    as an optional tag, see --sam-bam-tags. If set to uniq than the sequence is omitted
    iff it is identical to the previous match's subsequence.
  type:
  - 'null'
  - string
- default: AS NM ZE ZI ZF
  doc: Write the specified optional columns to the SAM/BAM file. Call --sam-bam-tags
    help for more details.
  id: param_sam-bam-tags
  inputBinding:
    prefix: -sam-bam-tags
  label: Write the specified optional columns to the SAM/BAM file. Call --sam-bam-tags
    help for more details.
  type:
  - 'null'
  - string
- default: hard
  doc: Whether to hard-clip or soft-clip the regions beyond the local match. Soft-clipping
    retains the full sequence in the output file, but obviously uses more space.
  id: param_sam-bam-clip
  inputBinding:
    prefix: -sam-bam-clip
  label: Whether to hard-clip or soft-clip the regions beyond the local match. Soft-clipping
    retains the full sequence in the output file, but obviously uses more space.
  type:
  - 'null'
  - string
- default: '8'
  doc: number of threads to run concurrently.
  id: param_threads
  inputBinding:
    prefix: -threads
  label: number of threads to run concurrently.
  type:
  - 'null'
  - int
- default: none
  doc: controls double-indexing.
  id: param_query-index-type
  inputBinding:
    prefix: -query-index-type
  label: controls double-indexing.
  type:
  - 'null'
  - string
- default: blastx
  doc: Blast Operation Mode.
  id: param_program
  inputBinding:
    prefix: -program
  label: Blast Operation Mode.
  type:
  - 'null'
  - string
- default: '1'
  doc: The translation table to use for nucl -> amino acid translation(not for BlastN,
    BlastP). See https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c for
    ids (default is generic). Six frames are generated.
  id: param_genetic-code
  inputBinding:
    prefix: -genetic-code
  label: The translation table to use for nucl -> amino acid translation(not for BlastN,
    BlastP). See https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c for
    ids (default is generic). Six frames are generated.
  type:
  - 'null'
  - int
- default: murphy10
  doc: Alphabet Reduction for seeding phase (ignored for BLASTN).
  id: param_alphabet-reduction
  inputBinding:
    prefix: -alphabet-reduction
  label: Alphabet Reduction for seeding phase (ignored for BLASTN).
  type:
  - 'null'
  - string
- default: '10'
  doc: Length of the seeds (default = 14 for BLASTN).
  id: param_seed-length
  inputBinding:
    prefix: -seed-length
  label: Length of the seeds (default = 14 for BLASTN).
  type:
  - 'null'
  - int
- default: '10'
  doc: Offset for seeding (if unset = seed-length, non-overlapping; default = 5 for
    BLASTN).
  id: param_seed-offset
  inputBinding:
    prefix: -seed-offset
  label: Offset for seeding (if unset = seed-length, non-overlapping; default = 5
    for BLASTN).
  type:
  - 'null'
  - int
- default: '1'
  doc: maximum seed distance.
  id: param_seed-delta
  inputBinding:
    prefix: -seed-delta
  label: maximum seed distance.
  type:
  - 'null'
  - int
- default: '2'
  doc: evaluate score of a region NUM times the size of the seed before extension
    (0 -> no pre-scoring, 1 -> evaluate seed, n-> area around seed, as well; default
    = 1 if no reduction is used).
  id: param_pre-scoring
  inputBinding:
    prefix: -pre-scoring
  label: evaluate score of a region NUM times the size of the seed before extension
    (0 -> no pre-scoring, 1 -> evaluate seed, n-> area around seed, as well; default
    = 1 if no reduction is used).
  type:
  - 'null'
  - int
- default: '2.0'
  doc: minimum average score per position in pre-scoring region.
  id: param_pre-scoring-threshold
  inputBinding:
    prefix: -pre-scoring-threshold
  label: minimum average score per position in pre-scoring region.
  type:
  - 'null'
  - double
- default: 'on'
  doc: filter hits that will likely duplicate a match already found.
  id: param_filter-putative-duplicates
  inputBinding:
    prefix: -filter-putative-duplicates
  label: filter hits that will likely duplicate a match already found.
  type:
  - 'null'
  - string
- default: 'on'
  doc: If the maximum number of matches per query are found already, stop searching
    if the remaining realm looks unfeasable.
  id: param_filter-putative-abundant
  inputBinding:
    prefix: -filter-putative-abundant
  label: If the maximum number of matches per query are found already, stop searching
    if the remaining realm looks unfeasable.
  type:
  - 'null'
  - string
- default: '62'
  doc: use '45' for Blosum45; '62' for Blosum62 (default); '80' for Blosum80; [ignored
    for BlastN]
  id: param_scoring-scheme
  inputBinding:
    prefix: -scoring-scheme
  label: use '45' for Blosum45; '62' for Blosum62 (default); '80' for Blosum80; [ignored
    for BlastN]
  type:
  - 'null'
  - int
- default: '-1'
  doc: Score per gap character (default = -2 for BLASTN).
  id: param_score-gap
  inputBinding:
    prefix: -score-gap
  label: Score per gap character (default = -2 for BLASTN).
  type:
  - 'null'
  - int
- default: '-11'
  doc: Additional cost for opening gap (default = -5 for BLASTN).
  id: param_score-gap-open
  inputBinding:
    prefix: -score-gap-open
  label: Additional cost for opening gap (default = -5 for BLASTN).
  type:
  - 'null'
  - int
- default: '2'
  doc: Match score [only BLASTN])
  id: param_score-match
  inputBinding:
    prefix: -score-match
  label: Match score [only BLASTN])
  type:
  - 'null'
  - int
- default: '-3'
  doc: Mismatch score [only BLASTN]
  id: param_score-mismatch
  inputBinding:
    prefix: -score-mismatch
  label: Mismatch score [only BLASTN]
  type:
  - 'null'
  - int
- default: '30'
  doc: Stop Banded extension if score x below the maximum seen (-1 means no xdrop).
  id: param_x-drop
  inputBinding:
    prefix: -x-drop
  label: Stop Banded extension if score x below the maximum seen (-1 means no xdrop).
  type:
  - 'null'
  - int
- default: '-3'
  doc: Size of the DP-band used in extension (-3 means log2 of query length; -2 means
    sqrt of query length; -1 means full dp; n means band of size 2n+1)
  id: param_band
  inputBinding:
    prefix: -band
  label: Size of the DP-band used in extension (-3 means log2 of query length; -2
    means sqrt of query length; -1 means full dp; n means band of size 2n+1)
  type:
  - 'null'
  - int
label: the Local Aligner for Massive Biological DatA
outputs:
- doc: File to hold reports on hits (.m* are blastall -m* formats; .m8 is tab-seperated,
    .m9 is tab-seperated with with comments, .m0 is pairwise format).
  id: param_output
  label: File to hold reports on hits (.m* are blastall -m* formats; .m8 is tab-seperated,
    .m9 is tab-seperated with with comments, .m0 is pairwise format).
  outputBinding:
    glob: $(inputs.param_output_filename)
  type:
  - 'null'
  - File
