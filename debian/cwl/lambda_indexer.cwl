#!/usr/bin/env cwl-runner

# This CWL file was automatically generated using CTDConverter.
# Visit https://github.com/WorkflowConversion/CTDConverter for more information.

baseCommand: lambda_indexer
class: CommandLineTool
cwlVersion: v1.0
doc: "Lambda is a local aligner optimized for many query sequences and searches in\
  \ protein space. It is compatible to BLAST, but much faster than BLAST and many\
  \ other comparable tools.\nDetailed information is available in the wiki: <https://github.com/seqan/lambda/wiki>\n\
  This is the indexer_binary for creating lambda-compatible databases.\n\n\n\nFor\
  \ more information, visit http://www.seqan.de"
inputs:
- default: 'false'
  doc: Display the help message with advanced options.
  id: param_full-help
  inputBinding:
    prefix: -full-help
  label: Display the help message with advanced options.
  type:
  - 'null'
  - string
- default: '1'
  doc: 'Turn this option off to disable version update notifications of the application. '
  id: param_version-check
  inputBinding:
    prefix: -version-check
  label: 'Turn this option off to disable version update notifications of the application. '
  type:
  - 'null'
  - string
- default: 'false'
  doc: Display long copyright information.
  id: param_copyright
  inputBinding:
    prefix: -copyright
  label: Display long copyright information.
  type:
  - 'null'
  - string
- default: '1'
  doc: 'Display more/less diagnostic output during operation: 0 [only errors]; 1 [default];
    2 [+run-time, options and statistics].'
  id: param_verbosity
  inputBinding:
    prefix: -verbosity
  label: 'Display more/less diagnostic output during operation: 0 [only errors]; 1
    [default]; 2 [+run-time, options and statistics].'
  type:
  - 'null'
  - int
- doc: Database sequences.
  id: param_database
  inputBinding:
    prefix: -database
  label: Database sequences.
  type: File
- doc: SEG intervals for database(optional).
  id: param_segfile
  inputBinding:
    prefix: -segfile
  label: SEG intervals for database(optional).
  type:
  - 'null'
  - File
- default: fm
  doc: Suffix array or full-text minute space.
  id: param_db-index-type
  inputBinding:
    prefix: -db-index-type
  label: Suffix array or full-text minute space.
  type:
  - 'null'
  - string
- default: 'on'
  doc: Truncate IDs at first whitespace. This saves a lot of space and is irrelevant
    for all LAMBDA output formats other than BLAST Pairwise (.m0).
  id: param_truncate-ids
  inputBinding:
    prefix: -truncate-ids
  label: Truncate IDs at first whitespace. This saves a lot of space and is irrelevant
    for all LAMBDA output formats other than BLAST Pairwise (.m0).
  type:
  - 'null'
  - string
- default: blastx
  doc: Blast Operation Mode.
  id: param_program
  inputBinding:
    prefix: -program
  label: Blast Operation Mode.
  type:
  - 'null'
  - string
- default: '1'
  doc: The translation table to use (not for BlastN, BlastP). See https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c
    for ids (default is generic).
  id: param_genetic-code
  inputBinding:
    prefix: -genetic-code
  label: The translation table to use (not for BlastN, BlastP). See https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c
    for ids (default is generic).
  type:
  - 'null'
  - int
- default: murphy10
  doc: Alphabet Reduction for seeding phase (ignored for BLASTN).
  id: param_alphabet-reduction
  inputBinding:
    prefix: -alphabet-reduction
  label: Alphabet Reduction for seeding phase (ignored for BLASTN).
  type:
  - 'null'
  - string
- default: radixsort
  doc: Algorithm for SA construction (also used for FM; see Memory  Requirements below!).
  id: param_algorithm
  inputBinding:
    prefix: -algorithm
  label: Algorithm for SA construction (also used for FM; see Memory  Requirements
    below!).
  type:
  - 'null'
  - string
- default: '8'
  doc: number of threads to run concurrently (ignored if a == skew7ext).
  id: param_threads
  inputBinding:
    prefix: -threads
  label: number of threads to run concurrently (ignored if a == skew7ext).
  type:
  - 'null'
  - int
- default: /home/mcrusoe/debian/lambda-align
  doc: temporary directory used by skew, defaults to working directory.
  id: param_tmp-dir
  inputBinding:
    prefix: -tmp-dir
  label: temporary directory used by skew, defaults to working directory.
  type:
  - 'null'
  - string
label: indexer for creating lambda-compatible databases
